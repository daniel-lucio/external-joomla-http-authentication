<?php
define ("GROUP_ID", 8);

openlog('joomla-auth', LOG_PID | LOG_PERROR, LOG_LOCAL0);
if (!isset($_SERVER['PHP_AUTH_USER'])) {
	syslog(LOG_NOTICE, "Authorization Required");
	header('WWW-Authenticate: Basic realm="OKays CNAM"');
	header('HTTP/1.0 401 Authorization Required');
	header("Content-Type: text/html");
	$content = 'Authorization Required';
	header("Content-Length: ".strval(strlen($content)));
	echo $content;
}
else{
	if (version_compare(PHP_VERSION, '5.3.1', '<')) {
		die('Your host needs to use PHP 5.3.1 or higher to run this version of Joomla!');
	}

	define('JOOMLA_RELATIVE_PATH', '/../');
	define('_JEXEC', 1);
 	set_include_path(get_include_path() . PATH_SEPARATOR . JOOMLA_RELATIVE_PATH);
 	if (file_exists(__DIR__ . JOOMLA_RELATIVE_PATH . 'defines.php')) {
		include_once __DIR__ . '/defines.php';
	}

	if (!defined('_JDEFINES'))  {
		define('JPATH_BASE', __DIR__.JOOMLA_RELATIVE_PATH);
		require_once JPATH_BASE . '/includes/defines.php';
	}

	require_once JPATH_BASE . '/includes/framework.php';

	$credentials['username'] = $_SERVER['PHP_AUTH_USER'];
	$credentials['password'] = $_SERVER['PHP_AUTH_PW'];
	syslog(LOG_NOTICE, 'u: '.$credentials['username'].' p: '.$credentials['password']);

	$db = JFactory::getDbo();
	 $query = $db->getQuery(true)->
        select('id, password')->from('#__users AS u')->
        join('INNER', $db->quoteName('#__user_usergroup_map', 'g') . ' ON u.id = g.user_id')->
        where('username=' . $db->quote($credentials['username']) . ' AND  g.group_id='.GROUP_ID);
	$db->setQuery($query);
	$result = $db->loadObject();

	if ($result){
		$match = JUserHelper::verifyPassword($credentials['password'], $result->password, $result->id);

		if ($match === true){
			// Bring this in line with the rest of the system
			$user = JUser::getInstance($result->id);
			echo 'Joomla! Authentication was successful!';
			syslog(LOG_NOTICE, 'Joomla! Authentication was successful!');
		}
		else {
			header('HTTP/1.0 401 Unauthorized');
			header('WWW-Authenticate: Basic realm="OKays CNAM"');
			unset($_SERVER['PHP_AUTH_USER'],$_SERVER['PHP_AUTH_PW']);
			$content = 'Unauthorized';
			header('Content-Length: '.strval(strlen($content)));
			echo $content.print_r($credentials,true);
			syslog(LOG_NOTICE, $content);
		}
	}
	else {
		// Invalid user
		syslog(LOG_NOTICE, 'Cound not find user in the database');
	}
}
closelog();